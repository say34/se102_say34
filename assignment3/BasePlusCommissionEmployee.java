//BaseCommissionEmployee class inherits from CommissionEmployee
//and accesses the superclass's private data via inherited
//public methods 

public class BasePlusCommissionEmployee extends CommissionEmployee {

	//base salary per week 
		private double baseSalary;
	
	public BasePlusCommissionEmployee(String firstName, String lastName,
			String socialSecurityNumber, double grossSales,
			double commissionRate,double baseSalary) {
		super(firstName, lastName, socialSecurityNumber, grossSales, commissionRate);
		// TODO Auto-generated constructor stub
		
		//if baseSalary is invalid throw exception
		if (baseSalary < 0.0)
			throw new IllegalArgumentException("Base salary must be >=0.0");
		this.baseSalary = baseSalary;
		
	
		
	}

	
	


	//set base Salary 
	public void setBaseSalary(double baseSalary)
	{
		
		if(baseSalary < 0.0)
			throw new IllegalArgumentException("Base salary must be >=0.0");
		this.baseSalary = baseSalary;
	}
	
	//return base salary
	public double getBaseSalary()
	{
		return baseSalary;
	}
	
	//calculate earnings 
	@Override // indicates that this method overrides a superclass method 
	public double earnings()
	{
		return getBaseSalary() + super.earnings();
	}
	
	//return String representation of BasePLusCommissionEmployee
	@Override
	public String toString()
	{
		return String.format("%s %s%n%s: %.2f", "base-salaried",super.toString(), "base salary",getBaseSalary());
	}





	





	
	
	
	

}
