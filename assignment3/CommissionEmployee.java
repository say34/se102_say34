//Sumant Yavatkar 
// 1/27/2015

public class CommissionEmployee extends Employee {

	//declare variables 
	
	private double grossSales; //gross weekly sales
	private double commissionRate; //commission percentage 
	
	//five argument constructor Commission Employees
	public CommissionEmployee(String firstName, String lastName, String socialSecurityNumber, double grossSales, double commissionRate)
	{
		// call to employee's constructor occurs here 
		super(firstName,lastName,socialSecurityNumber);
		
		//if grossSales is invalid throw exception 
		if(grossSales < 0.0 )
			throw new IllegalArgumentException("Gross sales must be >=0.0");
		
		//if commissionRate is invalid throw Exception 
		if(commissionRate<= 0.0 && commissionRate >= 1.0)
			throw new IllegalArgumentException("Commission rate must be > 0.0 and < 1.0");
		
		//this.firstName = firstName;
		//this.lastName = lastName;
		//this.socialSecurityNumber = socialSecurityNumber;
		this.grossSales = grossSales;
		this.commissionRate = commissionRate;
		
		
	} //end constructor
	
	//return first name
	/*public String getFirstName()
	{
		return firstName;
	}
	
	//return last name
	public String getLastName()
	{
		return lastName;
	}
	
	//get Social Security number
	public String getSocialSecurityNumber(){
		return socialSecurityNumber;
	}
	*/
	
	
	//set gross Sales amount
	public void setGrossSales(double grossSales)
	{
		if(grossSales <0.0)
			throw new IllegalArgumentException("Gross sales must be >= 0.0");
		this.grossSales = grossSales;
	}
	
	//return gross Sales amount 
	public double getGrossSales()
	{
		return grossSales;
	}
	
	//set commissionRate
	public void setCommissionRate(double commissionRate)
	{
		if(commissionRate <=0.0 || commissionRate >=1.0)
			throw new IllegalArgumentException("Commission rate must be >0.0 and <1.0");
		this.commissionRate = commissionRate;
	}
	
	//return commissionRate
	public double getCommissionRate()
	{
		return commissionRate;
	}
	
	//calculate Earnings 
	public double earnings()
	{
		return getCommissionRate() * getGrossSales();
	}
	
	
	//return String representation of CommissionEmployee object
	 //indicates that this method overrides a superclass method 
	public String toString()
	{
		return String.format("%s: %s%n%s: %.2f%n%s: %.2f", "commission employee", super.toString(),"gross sales",getGrossSales(),"commission rate", getCommissionRate());
		//return String.format("%s: %s %s%n%s: %s%n%s: %.2f%n%s: %.2f", "commission employee",getFirstName(), getLastName(), "social security number", getSocialSecurityNumber(),"gross sales",getGrossSales(),"commission rate", getCommissionRate());
	}
	
	
	

}
