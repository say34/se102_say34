
public class CommissionEmployeeTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		CommissionEmployee employee = new CommissionEmployee("Sumant","Yavatkar","123-34-5678",50.20,20.40);
		
		//get commission employee data
		System.out.println("Employee information obtained by get methods:");
		System.out.printf("%n%s %s%n", "First name is",employee.getFirstName());
		System.out.printf("%s %s%n", "Last name is", employee.getLastName());
		System.out.printf("%s %s%n", "Social Security number is", employee.getSocialSecurityNumber());
		System.out.printf("%s %.2f%n", "Gross Sales is", employee.getGrossSales());
		System.out.printf("%s %.2f%n", "Commission Rate is", employee.getCommissionRate());

		//set gross sales 5000
		employee.setGrossSales(5000);
		employee.setCommissionRate(0.1);
		
		System.out.printf("%n%s:%n%n%s%n","Updated employee information obtained by toString",employee.toString());
		
	}

}
