//Sumant Yavatkar
// 1/27/2015



public class Employee {

	//Declare all variables 
	private final String firstName;
	private final String lastName;
	private final String socialSecurityNumber;
	
	
	//Employee  Argument Constructor
	public Employee(String firstName, String lastName, String socialSecurityNumber)
	{
		
		this.firstName = firstName;
		this.lastName = lastName;
		this.socialSecurityNumber = socialSecurityNumber;
	}
	
		//return first name
		public String getFirstName()
		{
			return firstName;
		}
		
		//return last name
		public String getLastName()
		{
			return lastName;
		}
		
		//get Social Security number
		public String getSocialSecurityNumber(){
			return socialSecurityNumber;
		}
	
		public String toString(){
			//return String.format("%s: %s %s%n%s: ", "commission employee", getFirstName(), getLastName(), "social security number", getSocialSecurityNumber());
			return String.format(" %s %s%n%s: %s", getFirstName(), getLastName(), "social security number", getSocialSecurityNumber());
		}
		

}
