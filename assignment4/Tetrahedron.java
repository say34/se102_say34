
public class Tetrahedron extends ThreeDimensionalShape{

	public Tetrahedron(double newlength) {
		super(newlength);
		super.setHeight(Math.sqrt(2.0));
		// TODO Auto-generated constructor stub
	}

	/*public Tetrahedon(double newlength) {
		super(newlength);
		
		// TODO Auto-generated constructor stub
	}*/

	public double getVolume()
	{
		//return (1.0/3.0) * super.getHeight() * getArea();
		return (1.0/3.0) * (((Math.sqrt(3))/4))*super.getLength()*super.getHeight();
	}

		public double getArea()
		{
		return Math.sqrt(3.0) *super.getLength() * super.getLength();		
		}

	public String toString()
	{
		return String.format("Tetrahedron : %n", super.getLength());
	}

	
	
	
}
