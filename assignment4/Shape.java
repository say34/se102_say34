
public abstract class Shape {

	//declare variables 
	private double width;
	private double length; 
		
	//abstract methods which will be completed in subclasses -> concrete class
	public abstract double getArea();
	
	
	public Shape(double length, double width)
	{
		//call methods 
		setLength(length);
		setWidth(width);
	}
	
	public Shape(double length)
	{
		setLength(length);
	}
	
	public void setLength(double newLength) {
		// TODO Auto-generated method stub
		length = newLength;
	}
	
	public double getLength()
	{
		return length;
	}

	public void setWidth(double newWidth)
	{
		width = newWidth;
	}
	
	
	public double getWidth()
	{
		return width;
	}
	
	
	
	
}
