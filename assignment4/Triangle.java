
public class Triangle extends TwoDimensionalShape {

	//Triangle Constructor 
	public Triangle(double newLength, double newWidth) {
		super(newLength, newWidth);
		// TODO Auto-generated constructor stub
	}

	
	//Calculate Area 
	public double getArea()
	{
		return super.getLength() * super.getWidth() * 0.5;
	}

	
	//Format Method 
	public String toString()
	{
		
		return String.format("Triangle %n Height: %.2f%nBase: %.2f%n", super.getLength(), super.getWidth());
	}
	
	 
	
}
