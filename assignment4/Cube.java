
public class Cube extends ThreeDimensionalShape {

	
	//Cube Constructor 
	public Cube(double newLength)
	{
		super(newLength);
	}
	
	
	//Calculate Volume 
	public double getVolume()
	{
		return super.getLength() * super.getLength() * super.getLength();
	}

	

	//Calculate Area 
	
	public double getArea() {
		// TODO Auto-generated method stub
		return super.getLength() * super.getLength();
	}

	
	//Format Method for String 
	public String toString()
	{
		return String.format("Cube %n", super.getLength());
	}
	
	
}
