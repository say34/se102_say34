
public class Sphere extends ThreeDimensionalShape{

	
	//Create Pi Variable and set it equal to math.pi =3.14159...
	private final double PI = Math.PI;
	
	
	//Constructor
	public Sphere(double newlength) {
		super(newlength);
		// TODO Auto-generated constructor stub
	}


	public double getVolume() {
		// TODO Auto-generated method stub
		
		//Calculate Volume of Sphere 
		return (4.0/3.0) * PI * super.getLength() * super.getLength() * super.getLength();
	}

	
	/*public double setVolume() {
		// TODO Auto-generated method stub
		return 0;
	}
	*/
	
	public double getArea() {
		// TODO Auto-generated method stub
		//Calculate Area if Sphere
		return 4*PI*super.getLength()*super.getLength();
	}

	
	//Format Method for String 
	public String toString()
	{
		return String.format("Sphere %n radius %.2f %n", super.getLength() );
	}


	
	
	
}
