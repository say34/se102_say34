
public abstract class ThreeDimensionalShape extends Shape {

	//Three- Dimensional Shapes Have Height
	private double height;
	
	//Declare Abstract Methods 
	public abstract double getVolume();
	
	public abstract double getArea();
	
	//constructor
	public ThreeDimensionalShape(double newlength) {
		super(newlength);
		// TODO Auto-generated constructor stub
	}

	public ThreeDimensionalShape(double newLength,double newWidth)
	{
		super(newLength,newWidth);
	}

	
	//constructors 
	public ThreeDimensionalShape(double newLength,double newWidth, double newHeight)
	{
		super(newLength,newWidth);
		setHeight(newHeight);
	}
	
	
	//get: set MEthods 
	public void setHeight(double newHeight)
	{
		height = newHeight;
	}

	public double getHeight()
	{
		return height;
	}
	
	

	
}
