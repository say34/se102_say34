
public class Square extends TwoDimensionalShape{

	
	
	//declare Area Variable
	public double Area; 
	
	
	//constructor 
	public Square(double newlength) {
		super(newlength);
		// TODO Auto-generated constructor stub
	}

	//return Area Calculation
	public double getArea() {
		return super.getLength() * super.getLength();
		
	}

	
	//Format Method
	public String toString()
	{
		return String.format("Square: %.2f %n", super.getLength());
	}
	
	
	
	

}
