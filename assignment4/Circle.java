
public class Circle extends TwoDimensionalShape {

	//Declare Pi Variable and Set it equal to 3.14
	private final double PI = Math.PI;
	
	
	//Constructor 
	public Circle(double newLength)
	{
		super(newLength);
	}
	
	
	//Calculate Area 
	public double getArea()
	{
		return PI * super.getLength() * super.getLength();
	}

	
	//Format Output 
	public String toString()
	{
		return String.format("Circle%n radius: %.2f%n", super.getLength());
	}
}
