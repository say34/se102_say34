//PROGRAM to test Shape area and Volvume 
//Sumant Yavatkar 

public class ShapeTest {

	public static void main(String[] args) {
		//Declare Shape Array
		Shape[] shapes = new Shape[6];
		
		//Two Dimensional Shapes 
		Circle circle = new Circle(5);
		Square square = new Square(4);
		Triangle triangle = new Triangle(1,3);
		//Three Dimensional Shapes 
		Sphere sphere = new Sphere(10);
		Cube cube = new Cube(5);
		Tetrahedron tetrahedron = new Tetrahedron(5);
		
		
		//Fill in Shape Array 
		shapes[0] = circle;
		shapes[1] = square;
		shapes[2] = triangle;
		shapes[3] = sphere;
		shapes[4] = cube;
		shapes[5] = tetrahedron;
		
		
		
		
		TwoDimensionalShape ShapeTwoD;
		ThreeDimensionalShape ShapeThreeD;
		
		System.out.println("Shapes Assignment Output");
		System.out.println();
		
		//Loop through Aray and print out volume and Area accordingly 
		for(Shape shape: shapes)
		{
			System.out.print(shape);
			//checks if the Shape is a Two Dimensional Shape
			//if not then its Three Dimensional Shape 
			if(shape instanceof TwoDimensionalShape)
			{
				ShapeTwoD = (TwoDimensionalShape) shape;
				System.out.println("The Area is " + ShapeTwoD.getArea());
				System.out.println("-------------");
			}
			
			else
			{
				ShapeThreeD = (ThreeDimensionalShape) shape;
				System.out.println("The Surface Area is: " + ShapeThreeD.getArea());
				System.out.println("The Volume is: " + ShapeThreeD.getVolume());
				System.out.println("-------------");
			}
			
		}
	}

}
