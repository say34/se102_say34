
import java.io.IOException;

class ExceptionA extends Exception
{
	
}

class ExceptionB extends ExceptionA
{
	
}
public class classException {

		public static void main(String[] args)
		{
			
			//Exception A test 
			try
			{
				throw new ExceptionA();
			}
			catch(Exception exception)
			{
				System.out.println(exception);
			}
			
			
			//Try catch system 
			try
			{
				throw new ExceptionB();
			}
			catch(Exception exception)
			{
				System.out.println(exception);
			}
		
			
			//NullPointerException
			try
			{
				throw new NullPointerException();
			}
			catch(Exception exception)
			{
				System.out.println(exception);
			}
			
			//IOException 
			try
			{
				throw new IOException();
			}
			catch(Exception exception)
			{
				System.out.println(exception);
			}
			
		}
}
