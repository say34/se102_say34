
public class rethrowException {
	
	//Method someMethod
	//Method someMethod2 declaration 
	
	public static void someMethod() throws Exception
	{
		try
		{
			someMethod2();
		}
		
		//catch exception method 
		catch(Exception exception2)
		{
			throw exception2;
		}
	}
	
	public static void someMethod2() throws Exception
	{
		//exception message 
		throw new Exception("Exception has thrown someMethod2 test");
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		try
		{
			someMethod();
		}
		catch(Exception exception)
		{
			//format error message 
			System.err.printf("%s/n",exception.getMessage());
			exception.printStackTrace();
		}
	}

}
