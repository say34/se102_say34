
public class PayableInterfaceTest {

	public static void main(String[] args) {
		
		//Create Array
		Payable[] payableObjects = new Payable[6];
		
		/*
		 employees[ 0 ] = new SalariedEmployee("Sumant", "Yavatkar", "123-45-6789", 123.00 );
	      employees[ 1 ] = new HourlyEmployee("Sum", "ANT", "123-45-6789", 123.45, 12 );
	      employees[ 2 ] = new CommissionEmployee("Hello", "World", "123-45-6789", 12345, .12 ); 
	      employees[ 3 ] = new BasePlusCommissionEmployee("RANDOM", "TROLL", "123-45-6789", 1234, .12, 123 );
	      employees[ 4 ] = new PieceWorker("Khaleesi", "PIZZA", "123-45-6789", 1.23, 123 );
	     // employees[ 4 ] = new PieceWorker2("Rick", "Bridges", "555-55-5555", 2.25, 400 );
	     System.out.println("Employee Data ");
	      
	     System.out.println();
	     System.out.println("--------------------");
	      //Loop through employee array containing objects and print out Employee Data 
	     
	     
	      for ( Employee currentEmployee : employees ) 
	      {  
	    	 System.out.println("Current Progress of Employee Type ");
	         System.out.println( currentEmployee ); 
	         System.out.printf("earnedcash $%,.2f\n\n", currentEmployee.earnings() );
	         System.out.println("------------------------------------");
	         
	         
	      } 
		*/
		
		 payableObjects[ 0 ] = new Invoice( "01234", "seat", 2, 375.00 );
	      payableObjects[ 1 ] = new Invoice( "56789", "tire", 4, 79.95 );
	      payableObjects[ 2 ] = new SalariedEmployee( "John", "Smith", "111-11-1111", 800.00 );
	      payableObjects[ 3 ] = new HourlyEmployee( "Karen", "Price", "222-22-2222", 16.75, 40 );
	      payableObjects[ 4 ] = new CommissionEmployee( "Sue", "Jones", "333-33-3333", 10000, .06 );
	      payableObjects[ 5 ] = new BasePlusCommissionEmployee( "Bob", "Lewis", "444-44-4444", 5000, .04, 300 );

	      System.out.println("Invoices and Employees processed polymorphically:\n" ); 

	// Loop through array of Payable objects 
	      for ( Payable currentPayable : payableObjects )
	      {
	        
	         	         
	         System.out.println(currentPayable.toString());
	            
	         if ( currentPayable instanceof BasePlusCommissionEmployee )
	         {
	          
	            BasePlusCommissionEmployee employee = ( BasePlusCommissionEmployee ) currentPayable;

	            double originalBaseSalary = employee.getBaseSalary();
	            System.out.println("THe given base salary" + originalBaseSalary + "is increasing by 10%");
	            //increase basesalary to 10% to new base salary 
	            employee.setBaseSalary( 1.10 * originalBaseSalary );
	            System.out.printf("Therefore, the new Base Salary is: $%,.2f\n", employee.getBaseSalary() );
	         } 

	         	//Part of original Example test case in book 
	         System.out.printf( "%s: $%,.2f\n\n", "payment due", currentPayable.getPaymentAmount() ); 
	         
	         System.out.println("----------------------------------------");
	      } 
		
		
		
		

	}

}
