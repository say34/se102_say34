
public class SalariedEmployee extends Employee {

	private double weeklySalary;
	
	//constructor
	public SalariedEmployee(String firstName,String lastName, String socialSecurityNumber, double weeklySalary)
	{
		super(firstName,lastName, socialSecurityNumber);
		
		if(weeklySalary < 0.0)
			throw new IllegalArgumentException("Weekly salary must be >=0.0");
		this.weeklySalary = weeklySalary;
	}

	
	
	//set the salary
	public void setWeeklySalary(double weeklySalary)
	{
		if(weeklySalary < 0.0)
			throw new IllegalArgumentException("Weekly salary must be >= 0.0");
		this.weeklySalary = weeklySalary;
	}
	
	//calculate earning; implement interface Payable method that was
	//abstract in super class employee 
	//return salary
	public double getWeeklySalary()
	{
		return weeklySalary;
	}
	
	@Override
	public double getPaymentAmount() {
		// TODO Auto-generated method stub
		return getWeeklySalary();
	}
	
	
	
	//return String format
	@Override 
	public String toString()
	{
		return String.format("salaried employee: %s%n%s: $%,.2f", super.toString(),"weekly Salary", getWeeklySalary());
		
		
	}
}
