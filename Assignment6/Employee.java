
public abstract class Employee implements Payable {

	private String firstName;
	private String lastName;
	private  String socialSecurityNumber;
	
	//constructor 
	public Employee(String firstName, String lastName, String socialSecurityNumber)
	{
		this.firstName = firstName;
		this.lastName = lastName;
		this.socialSecurityNumber = socialSecurityNumber;
	}
	
	public void setFirstName(String firstName)
	{
		this.firstName = firstName;
	}
	
	//return first Name 
	public String getFirstName()
	{
		return firstName;
	}
	
	public void setLastName (String lastName)
	{
		this.lastName = lastName;
	}
	public String getLastName()
	{
		return lastName;
	}
	
	public void socialSecurityNumber(String socialSecurityNumber)
	{
		this.socialSecurityNumber = socialSecurityNumber;
	}
	
	public String getSocialSecurityNumber()
	{
		return socialSecurityNumber;
	}
	
	//toString Method format 
	@Override 
	public String toString()
	{
		return String.format("%s %s%nsocial security number: %s", getFirstName(),getLastName(),getSocialSecurityNumber());
	}
	
}
