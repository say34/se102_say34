//Sumant Yavatkar
//SE102: Assignment 2 Submission


import java.util.Scanner;

import java.util.ArrayList;

public class TotalSales {

	// declare variables
	public static int salesPerson;
	public static int productNum;
	public static int salesRow;
	public static int salesCol;
	public static double salesAmount;
	public static double totalSales[][];
	public static double personTotal[] = new double[4];
	public static double salesTotal = 0;
	public static double[][] sales;
	public static ArrayList<String> salesPeople = new ArrayList<String>();

	public static void main(String args[]) {
		totalSales = new double[5][4];
		@SuppressWarnings("resource")
		Scanner input = new Scanner(System.in);

		System.out.println("Enter a sales person number (-1 to end):  ");
		salesPerson = input.nextInt();

		while (salesPerson >= 1 && salesPerson <= 4) {
			System.out.println("Enter product number: ");
			productNum = (int) input.nextDouble();
			System.out.println("Enter sales amount: ");
			salesAmount = input.nextDouble();

			//Check if the User Input follows the program requirements
			//If the User Fails to meet requirements the following statements below are printed 
			
			
			
			if (salesPerson >= 1 && salesPerson <= 4 && productNum >= 1
					&& productNum <= 5 && salesAmount >= 0) {
				totalSales[productNum - 1][salesPerson - 1] += salesAmount;
			} else {
				System.out
						.println("Please follow the appropiate conditions otherwise the program will fail");
				System.out
						.println("There are only 5 products: choose one of the products from 1-5");
				System.out
						.println("There are only 4 salespersons from 1 to 4, nothing more ");
				System.out
						.println("Enter a dollar amount of either 0 or greater");
			}

			System.out.println("Enter salesperson number (-1 to end): ");
			salesPerson = input.nextInt();

		}

		for (int i = 0; i < 4; i++)

		{
			personTotal[i] = 0;

		}

		
//Objective 2: Need to Format 
//ProductSalesperson 1Salesperson 2Salesperson 3Salesperson 410.0  0.0  0.0  2.0  2.023.0  0.0  0.0  0.0  5.030.0  0.0  0.0  0.0  5.040.0  0.0  5.0  0.0  10.050.0  0.0  0.0  0.0  10.0Total3.00.05.02.0

		salesPeople.add("    Product ");
		salesPeople.add("Salesperson 1  ");
		salesPeople.add("Salesperson 2  ");
		salesPeople.add("Salesperson 3  ");
		salesPeople.add("Salesperson 4  ");
		salesPeople.add("Total  ");
	
		//Loop through Array List and print out sales people row (Product,Sales Person 1-4) 
		
		
		for (String temp : salesPeople) {
			System.out.printf(temp);

		}

		System.out.println(" ");
		System.out.println(" ");
		
		
		// Print each sales value of a product by looping through
		// each column and row
		
		
		//x is to loop through rows
		for (int x = 0; x < 5; x++) {
			System.out.printf("%8d              ",(x + 1));
			// i is  to loop through column
			for (int i = 0; i < 4; i++) {
				System.out.print(totalSales[x][i] + "         ");
				personTotal[i] += totalSales[x][i];
				salesTotal += totalSales[x][i];

			}

			
			System.out.println(salesTotal);
		}

		
		System.out.print("     Total            ");
			for(int i=0;i<4;i++)
				
				System.out.print(personTotal[i]+ "         ");
			System.out.println();
	}

}
