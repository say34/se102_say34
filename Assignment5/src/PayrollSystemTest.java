
public class PayrollSystemTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//Payroll Employee  Test
		
		//From previous test example in book ignore 
		//SalariedEmployee salariedEmployee = new SalariedEmployee("John", "Smith", "111-11-1111", 800.00); 
		//HourlyEmployee hourlyEmployee = new HourlyEmployee("Karen", "Price", "222-22-2222", 16.75, 40);
		//------------------------
		
		
		//Create Employee array with length 5 -> 5 employee objects 
	      Employee[] employees = new Employee[ 5 ]; 

	      
	      //create employee array 
	      employees[ 0 ] = new SalariedEmployee("Sumant", "Yavatkar", "123-45-6789", 123.00 );
	      employees[ 1 ] = new HourlyEmployee("Sum", "ANT", "123-45-6789", 123.45, 12 );
	      employees[ 2 ] = new CommissionEmployee("Hello", "World", "123-45-6789", 12345, .12 ); 
	      employees[ 3 ] = new BasePlusCommissionEmployee("RANDOM", "TROLL", "123-45-6789", 1234, .12, 123 );
	      employees[ 4 ] = new PieceWorker("Khaleesi", "PIZZA", "123-45-6789", 1.23, 300 );
	     // employees[ 4 ] = new PieceWorker2("Rick", "Bridges", "555-55-5555", 2.25, 400 );
	     System.out.println("Employee Data ");
	      
	     System.out.println();
	     System.out.println("--------------------");
	      //Loop through employee array containing objects and print out Employee Data 
	     
	     
	      for ( Employee currentEmployee : employees ) 
	      {  
	    	 System.out.println("Current Progress of Employee Type ");
	         System.out.println( currentEmployee ); 
	         System.out.printf("earnedcash $%,.2f\n\n", currentEmployee.earnings() );
	         System.out.println("------------------------------------");
	         
	         
	      } 
		//d
	      
		
	}

}
