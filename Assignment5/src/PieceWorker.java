

public class PieceWorker extends Employee {
	
	//declare variables 
	//wages perpiece
	//piece variable 
	private double wage; 
	   private int pieces; 

	
	   public PieceWorker(String firstName, String lastName,
				String socialSecurityNumber, double wage2, int pieces2 )
	   {
	      super( firstName, lastName, socialSecurityNumber );
	      setWage( wage2 ); 
	      setPieces( pieces2 ); 
	   } 

	   // set wage
	   public void setWage( double wage2 )
	   {
		   
		   //check if wages is greater than 0 
		   //if not warning is given 
	      if ( wage2 >= 0.0 )
	      {
	    	  wage = wage2;
	      }
	         
	      else
	         throw new IllegalArgumentException( "Wages per each  piece must be >= 0" );
	   } 

	   // return wage
	   public double getWage()
	   {
	      return wage;
	   } 

	   // set pieces 
	   public void setPieces( int pieces2 )
	   {
		   
		   //Checks of pieces is greater than 0 
	      if ( pieces2 >= 0.0 )
	      {
	    	  pieces = pieces2;
	      }
	        
	      else
	    	  
	         throw new IllegalArgumentException( "Pieces which are produced must be >= 0" );
	   } 
	   
	   

	   // return pieces that are produced 
	   public int getPieces()
	   {
	      return pieces;
	   } 

	   // calculate earnings
	   
	   
	   
	   @Override
	   public double earnings()
	   {
	      return getPieces() * getWage();
	   }
	   
	   
	   
	  //return String with appropiate format 
	   @Override
	   public String toString()
	   {
	      return String.format( "%s: %s\n%s: $%,.2f; %s: %d", "piece worker", super.toString(), "wage per piece", getWage(), "pieces produced", getPieces() );
	   }

	
	
}