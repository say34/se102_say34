

//Employee superclass 

public abstract class Employee {

	private final String firstName;
	private final String lastName; 
	private final String socialSecurityNumber;
	
	
	//constructor
	public Employee(String firstName, String lastName, String socialSecurityNumber)
	{
		this.firstName= firstName;
		this.lastName = lastName;
		this.socialSecurityNumber = socialSecurityNumber;
	}
	
	
	//return first name 
	public String getFirstName()
	{
		return firstName;
	}
	
	//returns last name 
	public String getLastName()
	{
		return lastName;
	}
	
	//return social security #
	public String getSocialSecurityNumber()
	{
		return socialSecurityNumber;
	}
	
	//String format 
	@Override
	public String toString()
	{
		return String.format("%s %s%nsocial security number: %s", getFirstName(),getLastName(),getSocialSecurityNumber());
	}
	
	//abstract method will be overriden by concrete subclass
	public abstract double earnings();
	
	
}
