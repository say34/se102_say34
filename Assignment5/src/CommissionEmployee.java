
public class CommissionEmployee extends Employee {

	private double grossSales; 
	private double commissionRate;
	
	//constructor 
	
	public CommissionEmployee(String firstName, String lastName,
			String socialSecurityNumber, double grossSales, 
			double commissionRate) 
	{
		super(firstName, lastName, socialSecurityNumber);
		// TODO Auto-generated constructor stub
		
		if (commissionRate <= 0.0 || commissionRate >= 1.0)
			throw new IllegalArgumentException("Commission rate must be > 0.0 and < 1.0");
		
		if(grossSales < 0.0) //validate 
			throw new IllegalArgumentException("Gross Sales must be >= 0.0");
		
		this.grossSales = grossSales; 
		this.commissionRate = commissionRate;
		
	}
	
	
	//set gross Sales amount
		public void setGrossSales(double grossSales)
		{
			if(grossSales <0.0)
				throw new IllegalArgumentException("Gross sales must be >= 0.0");
			this.grossSales = grossSales;
		}
		
		//return gross Sales amount 
		public double getGrossSales()
		{
			return grossSales;
		}
		
		//set commissionRate
		public void setCommissionRate(double commissionRate)
		{
			if(commissionRate <=0.0 || commissionRate >=1.0)
				throw new IllegalArgumentException("Commission rate must be >0.0 and <1.0");
			this.commissionRate = commissionRate;
		}
		
		//return commissionRate
		public double getCommissionRate()
		{
			return commissionRate;
		}
		
		//calculate Earnings 
		@Override
		public double earnings()
		{
			return getCommissionRate() * getGrossSales();
		}
		
		
		//return String representation of CommissionEmployee object
		 //indicates that this method overrides a superclass method 
		public String toString()
		{
			return String.format("%s: %s%n%s: %.2f%n%s: %.2f", "commission employee", super.toString(),"gross sales",getGrossSales(),"commission rate", getCommissionRate());
			//return String.format("%s: %s %s%n%s: %s%n%s: %.2f%n%s: %.2f", "commission employee",getFirstName(), getLastName(), "social security number", getSocialSecurityNumber(),"gross sales",getGrossSales(),"commission rate", getCommissionRate());
		}


	
	
	
}
