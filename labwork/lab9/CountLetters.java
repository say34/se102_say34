import java.util.*;

public class CountLetters {

	public static void main(String[] args)
	{
		try{
			int[] counts = new int[26];  
		
		 //get word from user    
		System.out.println("Please enter in a word ");
		Scanner input = new Scanner(System.in);
		String word = input.nextLine();
		 //convert to all upper case   
		word = word.toUpperCase();
		 //count frequency of each letter in string  for (int i=0; i < word.length(); i++)      counts[word.charAt(i)-'A']++;  
		for(int i=0;i<word.length();i++)
		{
			//Letter A recogition 
			counts[word.charAt(i)-'A']++;
		}
		//print frequencies   
		System.out.println("------");
		for(int j=0;j<counts.length;j++)
			if(counts[j]!=0)
				System.out.println((word.charAt(j)-'A') + ": " + counts[j]);
	
		
		}
		
	catch(Exception e)
		{
		System.out.println(e.getMessage());
		
		}
		}
	
}
