import java.util.Scanner;
public class SquareTest{

    public static void main(String args[]){

	int size; 				// size of next square
	Scanner in = new Scanner(System.in);
	// use -1 to indicate the end of input data
	// get size of next square
	size = in.nextInt(); 
	while (size != -1)
	{
	    // create a new Square of the given size
	    Square a = new Square(size);

	    // call its read method to read the values of the square
	    a.readSquare(in);

	    // print the square
	    a.printSquare();

	    // determine and print whether it is a magic square
	    System.out.println(a.magic());

	    // get the size of the next square
	    size = in.nextInt();
	}
	in.close();
    }
}
